package com.example.lab1;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextClock;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class MainFragment extends Fragment {

    private Button firstButton;
    private Button secondButton;

    public static String timeZoneValue = "GMT+0300";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container , false);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        firstButton = view.findViewById(R.id.main_button_first);
        secondButton = view.findViewById(R.id.main_button_second);
        TextClock textClock = view.findViewById(R.id.hk_time);

        textClock.setTimeZone(timeZoneValue);

        firstButton.setOnClickListener(v -> {
            menuHandler(new SecondaryFragment());
        });


        secondButton.setOnClickListener(v -> {
            menuHandler(new ThirdFragment());
        });

    }

    public void menuHandler(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(R.id.fragmentContainer, fragment)
                .commit();
    }

}
