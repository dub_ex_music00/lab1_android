package com.example.lab1;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextClock;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class SecondaryFragment extends Fragment {

    private Spinner spiner;
    private LinkedHashMap<String , SimpleTimeZone> zoneMap = new LinkedHashMap<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_secondary, container , false);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);

        spiner = view.findViewById(R.id.spinner_from);
        intitMap();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, new ArrayList<>(zoneMap.keySet()));
        spiner.setAdapter(adapter);

//        TextClock textClock = view.findViewById(R.id.hk_time);
//        textClock.setTimeZone("GMT+0800");
        spiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                MainFragment.timeZoneValue = String.valueOf(spiner.getSelectedItem());

                Toast.makeText(getContext(), "Timezone Selected " + spiner.getSelectedItem() , Toast.LENGTH_SHORT ).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
    public void intitMap() {
        for (int i = 12; i > 0; i--) {
            if (i >= 10) {
                zoneMap.put("GMT-" + String.valueOf(i) + "00", new SimpleTimeZone(360000 * i, String.valueOf(-i)));
            } else {
                zoneMap.put("GMT-0" + String.valueOf(i) + "00", new SimpleTimeZone(360000 * i, String.valueOf(-i)));
            }

        }
        for (int i = 0; i <= 12; i++) {
            if (i >= 10) {
                zoneMap.put("GMT+" + String.valueOf(i) + "00", new SimpleTimeZone(360000 * i, String.valueOf(i)));
            } else {
                zoneMap.put("GMT+0" + String.valueOf(i) + "00", new SimpleTimeZone(360000 * i, String.valueOf(i)));
            }
        }
    }


}
