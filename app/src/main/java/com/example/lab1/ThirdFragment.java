package com.example.lab1;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextClock;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class ThirdFragment extends Fragment {

    private Button startButton;
    private Button stopButton;
    private Button circleButton;
    private Button pauseButton;

    private Timer timer;
    private MyTimer myTimer;
    private TextView textView;
    private int isPaused = 0;

    private Calendar prevCircle;
    private Calendar currentTime;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_third, container , false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        startButton = view.findViewById(R.id.start);
        stopButton = view.findViewById(R.id.stop);
        circleButton = view.findViewById(R.id.circle);
        pauseButton = view.findViewById(R.id.pause);

        ListView list = view.findViewById(R.id.list);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, new LinkedList<>());

        list.setAdapter(adapter);


        textView = view.findViewById(R.id.timer);


            startButton.setOnClickListener(v -> {
                if (timer != null) {
                    timer.cancel();
                    timer = null;
                }
                isPaused = 0;
                pauseButton.setText("Пауза");
                adapter.clear();
                prevCircle = Calendar.getInstance();
                prevCircle.set(Calendar.HOUR_OF_DAY, 0 );
                prevCircle.set(Calendar.MINUTE, 0 );
                prevCircle.set(Calendar.SECOND, 0 );
                prevCircle.set(Calendar.MILLISECOND, 0 );
                timer = new Timer();
                myTimer = new MyTimer();
                currentTime = Calendar.getInstance();
                currentTime.set(Calendar.HOUR_OF_DAY, 0 );
                currentTime.set(Calendar.MINUTE, 0 );
                currentTime.set(Calendar.SECOND, 0 );
                currentTime.set(Calendar.MILLISECOND, 0 );

                timer.schedule(myTimer , 500 , 1000);

            });

        stopButton.setOnClickListener(v -> {
            if (timer != null) {
                timer.cancel();
                timer = null;
                adapter.clear();

            }
        });
        pauseButton.setOnClickListener(v -> {

            if (isPaused == 0){
                timer.cancel();
                timer.purge();
                timer = new Timer();
                myTimer = new MyTimer();
                isPaused = 1;
                pauseButton.setText("Продовжити");
            }else{
                timer.schedule(myTimer , 500 , 1000);
                isPaused = 0;
                pauseButton.setText("Пауза");
            }
        });

        circleButton.setOnClickListener(v -> {
            SimpleDateFormat  simpleDateFormat = new SimpleDateFormat("HH:mm:ss" , Locale.getDefault());
            Calendar calendar = Calendar.getInstance();


            calendar.set(Calendar.HOUR_OF_DAY, 0 );
            calendar.set(Calendar.MINUTE, 0 );
            calendar.set(Calendar.SECOND, 0 );
            calendar.set(Calendar.MILLISECOND, 0 );

            calendar.set(Calendar.SECOND , (currentTime.get(Calendar.SECOND) - prevCircle.get(Calendar.SECOND)));

            adapter.add(simpleDateFormat.format(calendar.getTime()));
            prevCircle = (Calendar) currentTime.clone();
        });

    }
    public class MyTimer extends TimerTask {
        @Override
        public void run() {
            currentTime.add(Calendar.SECOND , 1);
            SimpleDateFormat  simpleDateFormat = new SimpleDateFormat("HH:mm:ss" , Locale.getDefault());
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setText(simpleDateFormat.format(currentTime.getTime()));
                }
            });

        }
    }


}
